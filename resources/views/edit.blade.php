<!DOCTYPE html>
<html>
<head>
	<title>Edit Data</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="bodyadd">
	@foreach($mahasiswa as $p)

	<form action="/update" method="post">
		<div class="subtitle">Edit Data</div>
		<div  class="input-container">
			{{ csrf_field() }}
			<input class="input" type="hidden" name="id" value="{{ $p->id }}"> <br/>
			<input class="input" type="text" required="required" name="nama" placeholder="Nama" value="{{ $p->nama_mahasiswa }}"> <br/>
			<input class="input" type="number" required="required" name="nim" placeholder="NIM" value="{{ $p->nim_mahasiswa }}"> <br/>
			<input class="input" type="text" required="required" name="kelas" placeholder="Kelas" value="{{ $p->kelas_mahasiswa }}"> <br/>
			<input class="input" type="text" required="required" name="prodi" placeholder="Program Studi" value="{{ $p->prodi_mahasiswa }}"> <br/>
			<input class="input" type="text" required="required" name="fakultas" placeholder="Fakultas" value="{{ $p->fakultas_mahasiswa }}"> <br/>
			<input class="submit" type="submit" value="Simpan Data">
			<a class="cancel" href="/">Cancel</a>
		</div>
		</form>
	@endforeach
</body>
<style>
	h3{
    text-align: center;
    color: white;
    font-size: 50px;
}

body {
    background-image:url("https://i1.wp.com/undiksha.ac.id/wp-content/uploads/2016/10/FTK.jpg?ssl=1") ;
    backdrop-filter: brightness(0.35);
    background-size: 80% ;
    height: 600px;
}
.add{
    background-color: rgb(41, 41, 245);
    color: white;
    font-size: 20px;
    padding: 14px 25px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border-radius: 10px;
    margin-left: 1030px;
}

.add:hover, .add:active {
    background-color:rgb(137, 227, 250);
}

.table{
    text-align: center;
}

.table1{
    margin: 0 auto;
    width: 80%;
}

th{
    color: white;
    margin-left: 10px;
    margin-right: 10px;
    background-color: rgb(170, 105, 40);
    padding-top: 10px;
    padding-bottom: 10px;
}

td{
    padding-top: 3px;
    padding-bottom: 3px;
    background-color: rgb(250, 228, 186);
}

.nama{
    text-align: left;
    padding-left: 10px;
}

.table1 tr:nth-child(even){background-color: #f2f2f2;}

.table1 tr:hover {background-color: #ddd;}

.hapus{
    border-radius: 15px;
    background-color: #fc2929;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    color: white;
}
.hapus:hover, .hapus:active {
    background-color:rgb(253, 147, 165);  
}

.edit{
    border-radius: 15px;
    background-color:rgb(86, 201, 40);
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    color: white;
}
.edit:hover, .edit:active {
    background-color:rgb(117, 253, 124);  
}

.bodyadd{
    align-items: center;
    display: flex;
    justify-content: center;
}


form {
    background-color: #15172b;
    border-radius: 20px;
    box-sizing: border-box;
    height: 600px;
    padding: 20px;
    width: 700px;
}
.input-container {
    height: 50px;
    position: relative;
    width: 80%;
    margin-left: 10%;
}

input,
.cancel {
    margin-top: 20px;
    background-color: #60647c;
    border-radius: 12px;
    border: 0;
    box-sizing: border-box;
    color: white;
    font-size: 18px;
    height: 100%;
    outline: 0;
    padding: 4px 20px 0;
    width: 100%;
    font-family: arial;
}
.input::-webkit-input-placeholder{
	color:rgb(204, 204, 245);
}
.submit{
    display: inline-block;
    margin-top: 40px;
    width: 30%;
    margin-left: 54%;
}
.submit:hover, .submit:active {
    background-color:rgb(100, 132, 219);  
}
.subtitle {
    text-align: center;
    color: #eee;
    font-family: sans-serif;
    font-size: 24px;
    font-weight: 600;
    margin-top: 10px;
}

.cancel{
    width: 30%;
    text-decoration: none;
    padding: 17px 10px 13px 10px;
}
.cancel:hover, .cancel:active {
    background-color:rgb(100, 132, 219);  
}
</style>
</html>