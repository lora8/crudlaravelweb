<!DOCTYPE html>
<html>
<head>
	<title>Tambah Data</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body class="bodyadd">
	<form action="store" method="POST">
	@csrf
    @method("POST")
		<div class="subtitle">Tambah Data</div>
		<div class="input-container">
			<input class="input" type="text" name="nama" required="required" placeholder="Nama"> <br/>
			<input class="input" type="number" name="nim" required="required" placeholder="NIM"> <br/>
			<input class="input" type="text" name="kelas" required="required" placeholder="Kelas"> <br/>
			<input class="input" type="text" name="prodi" required="required" placeholder="Program Studi"> <br/>
			<input class="input" type="text" name="fakultas" required="required" placeholder="Fakultas"> <br/>
			<input class="submit" type="submit" value="Simpan Data">
			<a class="cancel" href="/">Cancel</a>
		</div>
	</form>
 
</body>
</html>