<!DOCTYPE html>
<html>
<head>
	<title>Data Mahasiswa</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</header>
</head>
<body>
	
	<div class="container ">
	<h3>Data mahasiswa</h3>
	<a class="add" href="/tambah">Tambah Data</a>
	
	<br/>
	<br/>
		<div class="table">
			<table class="table1" border="0">
				<tr>
					<th>Nama</th>
					<th>Nim</th>
					<th>Kelas</th>
					<th>Prodi</th>
					<th>Fakultas</th>
					<th>Opsi</th>
				</tr>
				@foreach($mahasiswa as $p)
				<tr class="data">
					<td class="nama">{{ $p->nama_mahasiswa }}</td>
					<td>{{ $p->nim_mahasiswa }}</td>
					<td>{{ $p->kelas_mahasiswa }}</td>
					<td>{{ $p->prodi_mahasiswa }}</td>
					<td>{{ $p->fakultas_mahasiswa }}</td>
					<td class="link">
						<a class="edit" href="/edit/{{ $p->id }}">Edit</a>
						<a class="hapus" href="/hapus/{{ $p->id }}">Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
		</div>
</body>
</html>